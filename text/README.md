# ExaSMR FY2019 Report - Document Sections

The body of the text is contained here. All chapters, sections, etc. are contained in this folder along with references to all associated figures, etc.


## Adding a section

To add a section to this document, load the [main file](main_main.tex) for the appropriate location in the text body (front, main, or end matter) and import a new section via `\subimport{./}{file_name}`, where `file_name` is a `.tex` file containing the text of the section you are adding. It is recommended to include the file early on in the drafting stage, and to re-render the document often (see the [main ReadMe](../ReadMe.md) for details). To start creating a new section, copy the [template](template.tex) file to whatever `file_name` you desire. Happy drafting!


## Questions?

Please email all questions to those listed in the [main ReadMe](../ReadMe.md), or to [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). We are happy to help in any way possible at any time!


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC). See [the acknowledgments file](main_matter/acknowledgments.tex) file for further details.

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>