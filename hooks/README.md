# Document Hooks

All hooks for the document are contained here. These files are loaded by the main document after initilizing various document properties and settings, allowing users to flexibily use and create documents.

The hooks include:
+ [details](details.tex): Contains document details, namely title, author, institute, and email definitions
+ [options](options.tex): Contains all document options that users may alter as desired
+ [commands](special_commands.tex): Allows end-user to create custom commands via `\newcommand{\mycmd}{cmd body}`. Commands entered here are available for use in the [details file](details.tex) file.
+ [title](title.tex): Allows end-users to create a custom title page via the `titlepage` environment. End-users may also use `\maketitle` to generate a quick title for the document if desired.

Other hooks exist for adding [acronyms](./acronyms.tex) and [glossary](./glossary.tex) entries. Use these files to add acronym and glossary entries. No acronyms or glossary entries/lists will be seen in the document if these hooks are unmodified. It is recommended to create special commands alongside the associated acronyms and glossaries to simplify their usage, i.e.

```latex
% Define the acronym
\newacronym{mc}{MC}{Monte-Carlo}

% Create a command to quickly reference the acronym
\newcommand{\mc}{\acrshort{mc}}
\newcommand{\mcfull}{\acrfull{mc}}
```


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).


## Questions

Questions or concerns may be directed to [Chase Juneau](mailto:junechas@isu.edu) or [Dr. Leslie Kerby](mailto:kerblesl@isu.edu).