# LaTeX Template Document

This repository contains a LaTeX template that is designed to be portable and re-usable.


## Generating the PDF

To build the LaTeX rendered [pdf file](main.pdf), users may run the [render](./render.sh) script, or they may type the following into a terminal:

```shell
pdflatex       main.tex
biblatex       main
makeglossaries main
pdflatex       main.tex
pdflatex       main.tex
```

The [render](./render.sh) script will do the above while supplying various options for the end-user. The script will automatically detect the first `.tex` file and assume it is the main LaTeX file to use. Usage of the script is as follows:
```latex
# Usage
./render.sh [-c] [-d output_file] [-f doc_name] [-h] [-v]

# Optional arguments
-c: Cleans all auxiliary files before and after compilation
-f: Name of main *.tex file (excluding *.tex); this may also be used as if a required argument
-d: set destination file for all LaTeX output
-h: Prints this help message and exits
-v: Uses verbose messaging
```

> Tip: The rendered [pdf file](main.pdf) can also be created by using the [TeXstudio IDE](https://www.texstudio.org/), as was done by the primary authors of the document. Note that TexStudio does *not* make a list of terms (glossary), by default.


## Contribute

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change. Contributions to this code implementation may be submitted via merge requests to this repository.

Prior to creating a merge request, please ensure the following:

+ Ensure all added features are properly documented within the code according to the commenting practices utilized here. Please keep all programming stylistically compliant with the remainder of the project.
+ Please ensure any install or build dependencies are removed.
+ Update the [ChangeLog](ChangeLog.md) file with details of changes to the interface, this includes new environment variables, exposed functions, useful file locations and container parameters, for example.
+ Merge requests may be merged in once the modifications have been reviewed and approved by all appropriate reviewers. The merge request will be merged once all comments made by the reviewer(s), if any, have been resolved and corrected accordingly.


## Questions?

Please email all questions to [Chase Juneau](mailto:junechas@isu.edu). I am happy to help in any way possible at any time!


## Acknowledgments

This work was derived from work supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="images/logos/ecp-logo.png" width=150>

<img src="images/logos/ISULogo.png" width=125>
<img src="images/logos/ornlLogo-Green.png" width=100>

