# ExaSMR FY19 Report - Document Template

All formatting utilized for the document is contained here, being imported from the [main TeX file](../exasmr_FY19_report.tex). This directory specifies all packages to be included and propogates the document options throughout the commands utilized.


## Questions?

Please email all questions to those listed above, or to [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). We are happy to help in any way possible at any time!


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC). See [the acknowledgments file](../text/main_matter/acknowledgments.tex) file for further details.

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>