#!/bin/bash -e
################################################################################
# Purpose: Renders the main pdf file
# Author: Chase M. Juneau
# Date: 2019-08-02
#
# PURPOSE: Renders the PDF file contained here for ease of user
#
# OPTIONS:
# -c : Cleans all aux files
# -d : Sets destination file for all LaTeX output
# -f : Name of main *.tex file (excluding *.tex)
# -h : Displays help message and exits
# -t : Kills pdflatex after [t] seconds
# -v : Verbose messaging
#
################################################################################
source "./include/trap.sh"
# Create directory in which the script exists
script_dir="$(dirname $(readlink -f $0))"

# Prints how to use the script
usage()
{
   echo ""
   echo "Usage: $0 [-c] [-d file_name] [-f doc_name] [-h] [-t second] [-v]"   >&2
   echo ""
   echo "   -c: Cleans all auxiliary files before and after compilation"
   echo "   -d: set destination file for all LaTeX output"
   echo "   -f: Name of main *.tex file (excluding *.tex)"
   echo "   -h: Prints this help message and exits"
   echo "   -t: Kills pdflatex after [t] seconds (default is 120)"
   echo "   -v: Uses verbose messaging"
   echo ""
}

# Prints the passed in message and stops
stopMsg()
{
   echo "$@"
   clean_aux
   exit 2
}

# Prints a message, applying the logical verbose filter
print ()
{
   if [ "${verbose}" == "true" ]; then
      echo "$@"
   fi
}

clean_files()
{
   file_glob="*${1}"
   files=( ${file_glob} )
   if [ -f "${files[0]}" ]; then
      print "      - Removing all *${1} files..."
      rm *${1}
   fi
}

clean_aux()
{
   # Validate option:
   if [ "${clean}" == "true" ]; then
      print "   - Cleaning all auxiliary files..."
      clean_files ".acn"
      clean_files ".acr"
      clean_files ".alg"
      clean_files ".aux"
      clean_files ".bbl"
      clean_files ".blg"
      clean_files "-blx.bib"
      clean_files ".glg"
      clean_files ".glo"
      clean_files ".gls"
      clean_files ".gz"
      clean_files ".ist"
      clean_files ".lof"
      clean_files ".log"
      clean_files ".lol"
      clean_files ".lot"
      clean_files ".out"
      clean_files ".toc"
      clean_files ".xml"
      clean_files ".log"
   fi
}

# Render the document
render ()
{
   # Clean auxiliary file
   clean_aux
   tex_file="$*"
   if [ ! -f "${tex_file}.tex" ]; then
      echo ""
      echo "Error: The provided filename (${tex_file}.tex) does not exist."
      echo "Error: Pleasre ensure the file exists in this directory."
      echo "Error: Exiting..."
      echo ""
      exit 2
   fi
   echo ""
   echo "- Rendering the PDF '${tex_file}.pdf'..."

   # Perform first build
   # NOTE: Printing the error below does NOT work when the timeout is exceeded!
   #       Instead, it simply exits the script (no messages).
   echo "   - Performing first pass..."
   timeout --signal=SIGHUP ${time_max}s pdflatex -halt-on-error "${tex_file}.tex" >> "${dest_file}"
   if [ $? == SIGHUP ] || [ ! -f "${tex_file}.pdf" ]; then
      echo ""
      echo "Error: Pdflatex could not render the document in ${time_max} sectons."
      echo "Error:    This could be due to loading too many images/text, or"
      echo "Error:    due to an error within the document. Please run pdflatex"
      echo "Error:    external to this script to determine cause of this timeout."
      echo "Error: Exiting..."
      echo ""
      exit 2
   fi

   # Load references
   echo "   - Loading references..."
   bibtex "${tex_file}"     >> "${dest_file}"

   # Create the list of terms/acronyms
   if [ -s ${tex_file}.glo ] || [ -s ${tex_file}.acn ]; then
      echo "   - Generating glossary..."
      makeglossaries "${tex_file}"     >> "${dest_file}"
   fi

   # Propogate references/terms/acronyms
   echo "   - Propogating references to the document..."
   pdflatex "${tex_file}.tex" >> "${dest_file}"

   # Ensure all hyperlinks that can be valid are
   echo "   - Ensuring all references and links are correct..."
   pdflatex "${tex_file}.tex" >> "${dest_file}"

   # Clean auxiliary file
   clean_aux

   echo "   - Done!"
}

# Toggles "clean" option
toggle_clean()
{
   if [ "${clean}" == "true" ]; then
      clean="false"
      print "The generated auxiliary files will not be removed."
   else
      clean="true"
      print "The generated auxiliary files will be removed."
   fi
}

# Toggles "clean" option
toggle_verbose()
{
   if [ "${verbose}" == "true" ]; then
      print "Disabling verbose messaging..."
      verbose="false"
   else
      verbose="true"
      print "Enabling verbose messaging..."
   fi
}

# Set document name
set_document_name()
{
   doc_name="$*"
   print "- Using '${doc_name}' as the main *.tex file."
}


# >>> DEFAULT VALUES FOR SCRIPT OPTIONS
clean="true"
dest_file="/dev/null"
time_max=120
verbose="false"
doc_name=""


# >>> VALIDATE OPTIONS
OPTIND=1
while getopts "cd:f:ht:v :" opt; do
   case "${opt}" in
      c) toggle_clean
         ;;
      d) dest_file="${OPTARG}"
         print "- All LaTeX-generated output will be directed to '${dest_file}'."
         ;;
      f) set_document_name "${OPTARG}"
         ;;
      h) usage
         exit 2
         ;;
      t) time_max=${OPTARG}
         print "- Timeout adjusted to ${time_max} seconds."
         ;;
      v) toggle_verbose
         ;;
      \?)
         "Invalid option: -${OPTARG}" >&2
         usage
         exit 2
         ;;
      *)
         usage
         exit 2
         ;;
   esac
done
print ""

# >>> LOAD NON-OPTIONAL ARGUMENTS
shift $((OPTIND-1))
if [ -z "${doc_name}" -a "${doc_name}" <> "" ]; then
   # If document hasn't been set, set it now
   set_document_name "$*"
fi
if [ -z "${doc_name}" ]; then
   # Verify the document name was set (via '-f' or after all arguments)
   echo "Warning: No main *.tex filename was given."
   file_glob="*.tex"
   files=( ${file_glob} )
   if [ -f "${files[0]}" ]; then
      set_document_name "${files[0]%.*}"
      echo "   - Trying '${doc_name}.tex' as the main .tex file."
   fi
fi

# >>> RENDER PDF
render "${doc_name}"

exit 0